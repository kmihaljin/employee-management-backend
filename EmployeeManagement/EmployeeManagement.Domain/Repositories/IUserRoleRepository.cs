﻿using EmployeeManagement.Common.Domain;
using EmployeeManagement.Domain.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Domain.Repositories
{
    public interface IUserRoleRepository : IRepository<UserRole, Guid>
    {
    }
}
