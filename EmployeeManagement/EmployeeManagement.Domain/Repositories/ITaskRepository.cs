﻿using EmployeeManagement.Common.Domain;
using EmployeeManagement.Domain.Models.Task;
using System;

namespace EmployeeManagement.Domain.Repositories
{
    public interface ITaskRepository : IRepository<Task, Guid>
    {

    }
}
