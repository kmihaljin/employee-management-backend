﻿using EmployeeManagement.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Domain.Models.Task
{
    public class Task : EntityBase<Guid>, IAggregateRoot
    {
        public string Subject { get; private set; }
        public string Body { get; private set; }
        public bool IsEmergency { get; private set; }
        public bool IsVisible { get; private set; }
        public DateTimeOffset Deadline { get; private set; }
        public DateTimeOffset Created { get; private set; }
        public DateTimeOffset? Modified { get; private set; }
        public bool Active { get; private set; }

        private Task()
        {

        }


        public static Task Create(
            string subject, 
            string body, 
            bool isEmergency, 
            bool isVisible, 
            DateTimeOffset deadline, 
            DateTimeOffset created)
        {
            return new Task
            {
                Subject = subject, 
                Body = body, 
                IsEmergency = isEmergency, 
                IsVisible = isVisible, 
                Deadline = deadline, 
                Created = created
            };
        }


    }
}
