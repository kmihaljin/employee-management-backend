﻿using EmployeeManagement.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Domain.Models.User
{
    public class UserRole : ValueObjectBase
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        private UserRole()
        {

        }

        public static UserRole Load(
            Guid id, 
            string code, 
            string name, 
            bool active)
        {
            return new UserRole
            {
                Id = id,
                Code = code,
                Name = name,
                Active = active
            };
        }

    }
}
