﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class User
    {
        public User()
        {
            AllowedLocations = new HashSet<AllowedLocation>();
            Events = new HashSet<Event>();
            TaskAssignedByNavigations = new HashSet<Task>();
            TaskAssignedToNavigations = new HashSet<Task>();
        }

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ImageUrl { get; set; }
        public Guid? OrganizationId { get; set; }
        public Guid UserRoleId { get; set; }
        public bool UseGeofencing { get; set; }
        public Guid? ExternalId { get; set; }
        public DateTimeOffset Created { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTimeOffset? Modified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Active { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual UserRole UserRole { get; set; }
        public virtual ICollection<AllowedLocation> AllowedLocations { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<Task> TaskAssignedByNavigations { get; set; }
        public virtual ICollection<Task> TaskAssignedToNavigations { get; set; }
    }
}
