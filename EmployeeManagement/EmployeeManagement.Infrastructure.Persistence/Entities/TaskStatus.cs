﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class TaskStatus
    {
        public TaskStatus()
        {
            Tasks = new HashSet<Task>();
        }

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public bool Active { get; set; }
        public string Color { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }
    }
}
