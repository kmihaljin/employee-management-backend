﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class TaskPriority
    {
        public TaskPriority()
        {
            Tasks = new HashSet<Task>();
        }

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int PriorityLevel { get; set; }
        public DateTimeOffset Created { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTimeOffset? Modified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }
    }
}
