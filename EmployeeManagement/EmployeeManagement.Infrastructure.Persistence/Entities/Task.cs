﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class Task
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Guid AssignedTo { get; set; }
        public Guid AssignedBy { get; set; }
        public Guid StatusId { get; set; }
        public Guid PriorityId { get; set; }
        public bool IsEmergency { get; set; }
        public bool IsVisible { get; set; }
        public DateTimeOffset Deadline { get; set; }
        public DateTimeOffset Created { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTimeOffset? Modified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Active { get; set; }

        public virtual User AssignedByNavigation { get; set; }
        public virtual User AssignedToNavigation { get; set; }
        public virtual TaskPriority Priority { get; set; }
        public virtual TaskStatus Status { get; set; }
    }
}
