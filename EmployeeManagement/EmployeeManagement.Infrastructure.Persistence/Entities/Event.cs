﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class Event
    {
        public Guid Id { get; set; }
        public Guid EventTypeId { get; set; }
        public Guid UserId { get; set; }
        public DateTimeOffset Occurred { get; set; }
        public DateTimeOffset Created { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTimeOffset? Modified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Active { get; set; }

        public virtual EventType EventType { get; set; }
        public virtual User User { get; set; }
    }
}
