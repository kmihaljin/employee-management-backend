﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class Organization
    {
        public Organization()
        {
            Users = new HashSet<User>();
        }

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ImageUrl { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTimeOffset Created { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTimeOffset? Modified { get; set; }
        public Guid? ModifiedBy { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
