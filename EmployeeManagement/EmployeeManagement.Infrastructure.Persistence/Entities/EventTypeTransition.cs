﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class EventTypeTransition
    {
        public Guid Id { get; set; }
        public Guid EventTypeId { get; set; }
        public Guid NextEventTypeId { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual EventType EventType { get; set; }
        public virtual EventType NextEventType { get; set; }
    }
}
