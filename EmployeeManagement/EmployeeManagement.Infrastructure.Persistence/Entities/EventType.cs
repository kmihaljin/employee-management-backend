﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EmployeeManagement.Infrastructure.Persistence.Entities
{
    public partial class EventType
    {
        public EventType()
        {
            EventTypeTransitionEventTypes = new HashSet<EventTypeTransition>();
            EventTypeTransitionNextEventTypes = new HashSet<EventTypeTransition>();
            Events = new HashSet<Event>();
        }

        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<EventTypeTransition> EventTypeTransitionEventTypes { get; set; }
        public virtual ICollection<EventTypeTransition> EventTypeTransitionNextEventTypes { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}
