﻿using MediatR.Pipeline;
using System.Threading;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Behaviours
{
    public class RequestLogger<TRequest> : IRequestPreProcessor<TRequest>
    {
        public RequestLogger()
        {

        }

        public Task Process(TRequest request, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }
    }
}
