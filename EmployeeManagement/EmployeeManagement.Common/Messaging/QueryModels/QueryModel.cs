﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Messaging.QueryModels
{
    

    /// <summary>
    /// Simple model for get entity meta data list -> using onliy with dapper 
    /// </summary>
    public class QueryModel
    {
        public string PropertyName { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }

        public string Query { get => $" {PropertyName} {Operator} {Value} "; }
    }
}
