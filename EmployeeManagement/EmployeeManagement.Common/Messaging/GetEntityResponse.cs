﻿using EmployeeManagement.Common.Messaging.Base;

namespace EmployeeManagement.Common.Messaging
{
    class GetEntityResponse<TEntity> : ResponseBase<GetEntityRequest>
    {
        public TEntity Entity { get; set; }
    }
}
