﻿using EmployeeManagement.Common.Messaging.Base;
using EmployeeManagement.Common.Messaging.QueryModels;
using System;
using System.Collections.Generic;

namespace EmployeeManagement.Common.Messaging
{
    public class GetEntityRequest : RequestBase
    {
        public long EntityId { get; set; }
        public Guid GuidEntityId { get; set; }
        public List<QueryModel> AdditionalQueryData { get; set; }
    }
}
