﻿using System;
using System.Runtime.Serialization;

namespace EmployeeManagement.Common.Messaging.Base
{
    [DataContract]
    public class ResponseBase<TRequest> : IResponseBase
    {
        [DataMember]
        public TRequest Request { get; set; }

        [DataMember]
        Guid IResponseBase.CorrelationId { get; set; }

        [DataMember]
        bool IResponseBase.Success { get; set; }

        [DataMember]
        string IResponseBase.Message { get; set; }
    }
}
