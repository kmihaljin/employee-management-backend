﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Messaging.Base
{
    public interface IResponseBase
    {
        Guid CorrelationId { get; set; }
        bool Success { get; set; }
        string Message { get; set; }
    }
}
