﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Messaging.Base
{
    public interface IRequestBase
    {
        Guid CorrelationId { get; set; }
    }
}
