﻿using EmployeeManagement.Common.Messaging.Base;

namespace EmployeeManagement.Common.Messaging
{
    class ManageEntityRequest<TEntity> : RequestBase
    {
        public TEntity EntityDto { get; set; }

    }
}
