﻿using EmployeeManagement.Common.Messaging.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Messaging
{
    class ManageEntityResponse<TRequest> : ResponseBase<TRequest>
    {
        public long EntityId { get; set; }
        public Guid GuidEntityId { get; set; }
    }
}
