﻿using System.Threading.Tasks;

namespace EmployeeManagement.Common.Domain
{
    public interface IEventDispatcher
    {
        //OR DispatchAsync
        Task Dispatch<T>(params T[] events) where T : IEvent;
    }
}
