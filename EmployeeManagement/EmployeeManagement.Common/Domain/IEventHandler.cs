﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Domain
{
    public interface IEventHandler<in T> where T: IEvent
    {
        //OR HandleAsync
        Task Handle(object @event);
    }
}
