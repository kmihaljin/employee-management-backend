﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Domain
{
    [DataContract(IsReference = true)]
    public abstract class EntityBase<TId>
    {
        [DataMember]
        public TId Id { get; set; }

        private readonly IDictionary<Type, IEvent> _events = new Dictionary<Type, IEvent>();
        public IEnumerable<IEvent> Events => _events.Values;

        public override bool Equals(object obj)
        {
            return obj is EntityBase<TId> @base && this == @base;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(EntityBase<TId> entity1, EntityBase<TId> entity2)
        {
            if ((object) entity1 == null && (entity2) == null)
            {
                return true;
            }

            if ((object)entity1 == null || (entity2) == null)
            {
                return false;
            }

            if (entity1.Id.ToString() == entity2.Id.ToString())
            {
                return true;
            }

            return false;
        }

        public static bool operator !=(EntityBase<TId> entity1, EntityBase<TId> entity2)
        {
            return !(entity1 == entity2);
        }

        public void AddEvent(IEvent @event)
        {
            _events[@event.GetType()] = @event;
        }

        public void ClearEvent()
        {
            _events.Clear();
        }
    }
}
