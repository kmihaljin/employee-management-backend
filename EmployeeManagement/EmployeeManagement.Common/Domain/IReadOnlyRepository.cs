﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Domain
{
    public interface IReadOnlyRepository<T, TId> 
    {
        Task<T> FindBy(TId id);
        Task<IEnumerable<T>> FindAll();
    }
}
