﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Common.Domain
{
    public interface IRepository<T, TId> : IReadOnlyRepository<T, TId>
    {
        Task<T> Save(T Entity);

        Task<T> Add(T Entity);

        Task Remove(T Entity);
    }
}
