﻿using EmployeeManagement.Common.Domain;
using EmployeeManagement.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeManagement.Infrastructure.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        public TaskRepository()
        {

        }

        public Domain.Models.Task.Task Add(Domain.Models.Task.Task Entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Domain.Models.Task.Task> FindAll()
        {
            throw new NotImplementedException();
        }

        public Domain.Models.Task.Task FindBy(Guid id)
        {
            
        }

        public void Remove(Domain.Models.Task.Task Entity)
        {
            throw new NotImplementedException();
        }

        public Domain.Models.Task.Task Save(Domain.Models.Task.Task Entity)
        {
            throw new NotImplementedException();
        }

        Task<Domain.Models.Task.Task> IRepository<Domain.Models.Task.Task, Guid>.Add(Domain.Models.Task.Task Entity)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<Domain.Models.Task.Task>> IReadOnlyRepository<Domain.Models.Task.Task, Guid>.FindAll()
        {
            throw new NotImplementedException();
        }

        Task<Domain.Models.Task.Task> IReadOnlyRepository<Domain.Models.Task.Task, Guid>.FindBy(Guid id)
        {
            throw new NotImplementedException();
        }

        System.Threading.Tasks.Task IRepository<Domain.Models.Task.Task, Guid>.Remove(Domain.Models.Task.Task Entity)
        {
            throw new NotImplementedException();
        }

        Task<Domain.Models.Task.Task> IRepository<Domain.Models.Task.Task, Guid>.Save(Domain.Models.Task.Task Entity)
        {
            throw new NotImplementedException();
        }
    }
}
