﻿using EmployeeManagement.Domain.Models.User;
using EmployeeManagement.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeManagement.Infrastructure.Repositories
{
    public class UserRoleRepository : IUserRoleRepository
    {
        private readonly EmployeeManagementContext _context;
        public UserRoleRepository(EmployeeManagementContext context)
        {
            _context = context;
        }

        public async Task<UserRole> Add(UserRole entity)
        {

            var userRole = new Persistence.Entities.UserRole
            {
                Active = true, 
                Code = entity.Code, 
                Name = entity.Name 
            };

            await _context.UserRoles.AddAsync(userRole).ConfigureAwait(false);

            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<IEnumerable<UserRole>> FindAll()
        {
            return await _context.UserRoles
                .Where(x => x.Active)
                .Select(x => UserRole.Load(x.Id, x.Code, x.Name, x.Active))
                .ToListAsync();
        }

        public async Task<UserRole> FindBy(Guid id)
        {
            var userRole = await _context.UserRoles.FindAsync(id);

            return UserRole.Load(
                 userRole.Id,
                 userRole.Code,
                 userRole.Name,
                 userRole.Active);

        }

        public async Task Remove(UserRole entity)
        {
            var userRole = await _context.UserRoles.FindAsync(entity.Id);

            userRole.Active = false;

            await _context.SaveChangesAsync();

        }

        public Task<UserRole> Save(UserRole Entity)
        {
            throw new NotImplementedException();
        }
    }
}
