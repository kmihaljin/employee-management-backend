﻿using EmployeeManagement.Infrastructure.Persistence.Entities;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Infrastructure
{
    public class EmployeeManagementContext : Context
    {
        public EmployeeManagementContext(DbContextOptions<Context> options) : base(options)
        {

        }
    }
}
