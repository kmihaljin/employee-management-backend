﻿using EmployeeManagement2.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagement2.Controllers
{
    [Route("api/event-type")]
    [ApiController]
    public class EventTypeController : ControllerBase
    {
        private readonly IEventTypeRepository _eventTypeRepository;
        public EventTypeController(IEventTypeRepository eventTypeRepository)
        {
            _eventTypeRepository = eventTypeRepository;
        }
        // GET: api/<EventTypeController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var eventTypes = await _eventTypeRepository.GetAllEventTypes();
            return Ok(eventTypes);
        }

        // GET api/<EventTypeController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var eventType = await _eventTypeRepository.GetEventType(id);
            return Ok(eventType);
        }

        // POST api/<EventTypeController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EventTypeController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EventTypeController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
