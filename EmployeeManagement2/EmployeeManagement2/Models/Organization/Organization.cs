﻿using EmployeeManagement2.Models.User;
using System;

namespace EmployeeManagement2.Models.Organization
{
    public class Organization
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ImageUrl { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTimeOffset Created { get; set; }
        public UserInfo CreatedBy { get; set; }
        public DateTimeOffset Modified { get; set; }
        public UserInfo ModifiedBy { get; set; }
        public bool Active { get; set; }
    }
}
