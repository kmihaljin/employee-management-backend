﻿using System;

namespace EmployeeManagement2.Models.Event
{
    public class EventType
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public bool IsDefault { get; set; }
        public bool Active { get; set; }

    }
}
