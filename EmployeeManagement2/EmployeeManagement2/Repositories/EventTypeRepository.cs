﻿using Dapper;
using EmployeeManagement2.Contracts;
using EmployeeManagement2.Models.Event;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EmployeeManagement2.Repositories
{
    public class EventTypeRepository : IEventTypeRepository
    {
        public async Task<List<EventType>> GetAllEventTypes()
        {
            using (var connection = new SqlConnection("Server=tcp:employee-management-db-server.database.windows.net,1433;Initial Catalog=EmployeeManagement;Persist Security Info=False;User ID=kmihalj;Password=L5YzrF6r;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
            {
                var eventTypes = await connection.QueryAsync<EventType>($"SELECT * FROM [dbo].[EventType]");
                return eventTypes.AsList();
            }
        }

        public async Task<EventType> GetEventType(Guid id)
        {
            using (var connection = new SqlConnection("Server=tcp:employee-management-db-server.database.windows.net,1433;Initial Catalog=EmployeeManagement;Persist Security Info=False;User ID=kmihalj;Password=L5YzrF6r;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
            {
                var eventType = await connection.QueryFirstAsync<EventType>($"SELECT * FROM [dbo].[EventType] WHERE Id = '{id}' ");
                return eventType;
            }
        }
    }
}
