﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement2.Repositories.DatabaseModels.User
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string ImageUrl { get; set; }
        public Guid? OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationCode { get; set; }
        public Guid? UserRoleId { get; set; }
        public string UserRoleName { get; set; }
        public string UserRoleCode { get; set; }
        public bool UseGeofencing { get; set; }
        public string ExternalId { get; set; }
        public DateTimeOffset Created { get; set; }
        public int? CreatedBy { get; set; }
        public string CreatedByCode { get; set; }
        public string CreatedByFirstName { get; set; }
        public string CreatedByLastName { get; set; }
        public string CreatedByFullName { get; set; }
        public DateTimeOffset Modified { get; set; }
        public int? ModifiedBy { get; set; }
        public string ModifiedByCode { get; set; }
        public string ModifiedByFirstName { get; set; }
        public string ModifiedByLastName { get; set; }
        public string ModifiedByFullName { get; set; }
        public bool Active { get; set; }

    }
}
