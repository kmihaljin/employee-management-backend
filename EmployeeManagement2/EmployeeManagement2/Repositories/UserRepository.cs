﻿using Dapper;
using EmployeeManagement2.Models.User;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement2.Repositories
{
    public class UserRepository
    {
        public List<User> GetUsersByOrganizationId(int organizationId)
        {
            using (var connection = new SqlConnection("Server=tcp:employee-management-db-server.database.windows.net,1433;Initial Catalog=EmployeeManagement;Persist Security Info=False;User ID=kmihalj;Password=L5YzrF6r;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
            {
                var documents = connection.Query<User>($"SELECT * FROM [dbo].[User] US WHERE US.OrganizationId = {organizationId}");
                return documents.AsList();
            }

        }
    }
}
