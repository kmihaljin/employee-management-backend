﻿using EmployeeManagement2.Models.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement2.Contracts
{
    public interface IEventTypeRepository
    {
        Task<List<EventType>> GetAllEventTypes();
        Task<EventType> GetEventType(Guid id);
    }
}
