--/****** Object:  Database [EmployeeManagement]    Script Date: 01/10/2021 18:43:19 ******/
--create DATABASE [EmployeeManagement] ;
--GO

USE [EmployeeManagement];
CREATE TABLE [dbo].[Organization](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](200) NULL,
	[ImageUrl] [nvarchar](1000) NULL,
	[Email] [nvarchar](100) NULL,
	[Phone] [nvarchar](25) NULL,
	[Created] [datetimeoffset](0) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[Modified] [datetimeoffset](0) NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](25) NULL,
	[Email] [nvarchar](100) NULL,
	[Address] [nvarchar](200) NULL,
	[ImageUrl] [nvarchar](1000) NULL,
	[OrganizationId] [uniqueidentifier] NULL,
	[UserRoleId] [uniqueidentifier] NOT NULL,
	[UseGeofencing] [bit] NOT NULL,
	[ExternalId] [nvarchar](1000) NULL,
	[Created] [datetimeoffset](0) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[Modified] [datetimeoffset](0) NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vUserDto]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vUserDto] as
select 
u.Id, 
u.Code,
u.FirstName, 
u.LastName, 
u.FirstName + ' ' + u.LastName as FullName, 
u.Phone, 
u.Email, 
u.Address, 
u.ImageUrl, 
u.OrganizationId, 
ORG.Name as OrganizationName, 
ORG.Code as OrganizationCode, 
u.UserRoleId,
ur.Name as UserRoleName, 
ur.Code as UserRoleCode, 
u.UseGeofencing, 
u.ExternalId, 
u.Created, 
u.CreatedBy,
UCR.Code as CreatedByCode, 
UCR.FirstName as CreatedByFirstName, 
UCR.LastName as CreatedByLastName, 
UCR.FirstName + ' ' + UCR.LastName as CreatedByFullName, 
u.Modified, 
u.ModifiedBy,
ULM.Code as ModifiedByCode, 
ULM.FirstName as ModifiedByFirstName, 
ULM.LastName as ModifiedByLastName, 
ULM.FirstName + ' ' + ULM.LastName as ModifiedByFullName, 
u.Active
from 
dbo.[User] U
left join dbo.[Organization] ORG on U.OrganizationId = ORG.Id
left join dbo.[UserRole] UR on U.UserRoleId = UR.Id
left join dbo.[User] UCR on U.CreatedBy = UCR.Id
left join dbo.[User] ULM on u.ModifiedBy = ULM.Id
GO
/****** Object:  Table [dbo].[AllowedLocation]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AllowedLocation](
	[Id] [uniqueidentifier] NOT NULL,
	[Latitude] [decimal](12, 9) NOT NULL,
	[Longitude] [decimal](12, 9) NOT NULL,
	[GeofenceRadiusMeters] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Created] [datetimeoffset](0) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[Modified] [datetimeoffset](0) NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_AllowedLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[Id] [uniqueidentifier] NOT NULL,
	[EventTypeId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Occurred] [datetimeoffset](0) NOT NULL,
	[Created] [datetimeoffset](0) NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[Modified] [datetimeoffset](0) NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventType]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventType](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventTypeTransition]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventTypeTransition](
	[Id] [uniqueidentifier] NOT NULL,
	[EventTypeId] [uniqueidentifier] NULL,
	[NextEventTypeId] [uniqueidentifier] NOT NULL,
	[Active] [bit] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_NextEventTypeAllowed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Task]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Task](
	[Id] [uniqueidentifier] NOT NULL,
	[Subject] [nvarchar](50) NOT NULL,
	[Body] [nvarchar](500) NOT NULL,
	[AssignedTo] [uniqueidentifier] NOT NULL,
	[AssignedBy] [uniqueidentifier] NOT NULL,
	[StatusId] [uniqueidentifier] NOT NULL,
	[PriorityId] [uniqueidentifier] NOT NULL,
	[IsEmergency] [bit] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[Deadline] [datetimeoffset](0) NOT NULL,
	[Created] [datetimeoffset](0) NOT NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[Modified] [datetimeoffset](0) NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskPriority]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskPriority](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PriorityLevel] [int] NOT NULL,
	[Created] [datetimeoffset](0) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[Modified] [datetimeoffset](0) NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_TaskPriority] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskStatus]    Script Date: 01/10/2021 18:43:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskStatus](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[Color] [nvarchar](255) NULL,
 CONSTRAINT [PK_TaskStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllowedLocation] ADD  CONSTRAINT [DF_AllowedLocation_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[AllowedLocation] ADD  CONSTRAINT [DF_AllowedLocation_Created]  DEFAULT (sysdatetimeoffset()) FOR [Created]
GO
ALTER TABLE [dbo].[Event] ADD  CONSTRAINT [DF_Event_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[EventType] ADD  CONSTRAINT [DF_EventType_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[EventTypeTransition] ADD  CONSTRAINT [DF_EventTypeTransition_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_Created]  DEFAULT (sysdatetimeoffset()) FOR [Created]
GO
ALTER TABLE [dbo].[Task] ADD  CONSTRAINT [DF_Task_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[TaskPriority] ADD  CONSTRAINT [DF_TaskPriority_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[TaskPriority] ADD  CONSTRAINT [DF_TaskPriority_Created]  DEFAULT (sysdatetimeoffset()) FOR [Created]
GO
ALTER TABLE [dbo].[TaskStatus] ADD  CONSTRAINT [DF_TaskStatus_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_Created]  DEFAULT (sysdatetimeoffset()) FOR [Created]
GO
ALTER TABLE [dbo].[UserRole] ADD  CONSTRAINT [DF_UserRole_Id]  DEFAULT (newsequentialid()) FOR [Id]
GO
ALTER TABLE [dbo].[AllowedLocation]  WITH CHECK ADD  CONSTRAINT [FK_AllowedLocation_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[AllowedLocation] CHECK CONSTRAINT [FK_AllowedLocation_User]
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_Event_EventType] FOREIGN KEY([EventTypeId])
REFERENCES [dbo].[EventType] ([Id])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_Event_EventType]
GO
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_Event_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_Event_User]
GO
ALTER TABLE [dbo].[EventTypeTransition]  WITH CHECK ADD  CONSTRAINT [FK_EventTypeTransition_EventType] FOREIGN KEY([EventTypeId])
REFERENCES [dbo].[EventType] ([Id])
GO
ALTER TABLE [dbo].[EventTypeTransition] CHECK CONSTRAINT [FK_EventTypeTransition_EventType]
GO
ALTER TABLE [dbo].[EventTypeTransition]  WITH CHECK ADD  CONSTRAINT [FK_NextEventTypeAllowed_EventType] FOREIGN KEY([EventTypeId])
REFERENCES [dbo].[EventType] ([Id])
GO
ALTER TABLE [dbo].[EventTypeTransition] CHECK CONSTRAINT [FK_NextEventTypeAllowed_EventType]
GO
ALTER TABLE [dbo].[EventTypeTransition]  WITH CHECK ADD  CONSTRAINT [FK_NextEventTypeAllowed_EventType1] FOREIGN KEY([NextEventTypeId])
REFERENCES [dbo].[EventType] ([Id])
GO
ALTER TABLE [dbo].[EventTypeTransition] CHECK CONSTRAINT [FK_NextEventTypeAllowed_EventType1]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_TaskPriority] FOREIGN KEY([PriorityId])
REFERENCES [dbo].[TaskPriority] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_TaskPriority]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_TaskStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[TaskStatus] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_TaskStatus]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_User] FOREIGN KEY([AssignedTo])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_User]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_User1] FOREIGN KEY([AssignedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_User1]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Organization]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserType] FOREIGN KEY([UserRoleId])
REFERENCES [dbo].[UserRole] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserType]
GO
ALTER DATABASE [EmployeeManagement] SET  READ_WRITE 
GO
