INSERT [dbo].[Organization] ([Id], [Code], [Name], [Address], [ImageUrl], [Email], [Phone], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'bb8436d0-3b03-ec11-981f-a04a5e948964', N'KAP', N'Kapeličanka', N'Kapelica 81 43280 Garešnica', NULL, N'kapelicanka@kapelicanka.com', N'+385916370118', CAST(N'2021-08-22T11:26:38.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[Organization] ([Id], [Code], [Name], [Address], [ImageUrl], [Email], [Phone], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'4ba73d0b-3c03-ec11-981f-a04a5e948964', N'MIHA', N'Mihaljinac d.o.o.', N'Kapelica 81 43280 Garešnica', NULL, N'mihaljinacdoo@gmail.com', N'+385915370118', CAST(N'2021-08-22T11:28:17.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[UserRole] ([Id], [Code], [Name], [Active]) VALUES (N'b11d9752-3c03-ec11-981f-a04a5e948964', N'ADMIN', N'Administrator', 1)
GO
INSERT [dbo].[UserRole] ([Id], [Code], [Name], [Active]) VALUES (N'b21d9752-3c03-ec11-981f-a04a5e948964', N'EMP', N'Zaposlenik', 1)
GO
INSERT [dbo].[User] ([Id], [Code], [FirstName], [LastName], [Phone], [Email], [Address], [ImageUrl], [OrganizationId], [UserRoleId], [UseGeofencing], [ExternalId], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'6fc402e6-3c03-ec11-981f-a04a5e948964', N'ADM1', N'Kristijan', N'Mihaljinac', N'+385916370118', N'kristijan.mihaljinac@gmail.com', N'Kapelica 81 43280 Garešnica', NULL, N'4ba73d0b-3c03-ec11-981f-a04a5e948964', N'b11d9752-3c03-ec11-981f-a04a5e948964', 0, NULL, CAST(N'2021-08-22T11:34:24.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[User] ([Id], [Code], [FirstName], [LastName], [Phone], [Email], [Address], [ImageUrl], [OrganizationId], [UserRoleId], [UseGeofencing], [ExternalId], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'5ab66609-3d03-ec11-981f-a04a5e948964', N'EMP1', N'Martin', N'Mihaljinac', N'+385915634887', N'martin.mihaljinac@gmail.com', N'Kapelica 81 43280 Garešnica', NULL, N'4ba73d0b-3c03-ec11-981f-a04a5e948964', N'b21d9752-3c03-ec11-981f-a04a5e948964', 1, NULL, CAST(N'2021-08-22T11:35:24.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[TaskPriority] ([Id], [Code], [Name], [PriorityLevel], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'56edd253-3d03-ec11-981f-a04a5e948964', N'LOWEST', N'Najniži', 0, CAST(N'2021-08-22T11:37:31.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[TaskPriority] ([Id], [Code], [Name], [PriorityLevel], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'be47005c-3d03-ec11-981f-a04a5e948964', N'LOW', N'Nizak', 1, CAST(N'2021-08-22T11:37:42.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[TaskPriority] ([Id], [Code], [Name], [PriorityLevel], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'b3704866-3d03-ec11-981f-a04a5e948964', N'MEDIUM', N'Srednji', 2, CAST(N'2021-08-22T11:38:00.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[TaskPriority] ([Id], [Code], [Name], [PriorityLevel], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'b56d6271-3d03-ec11-981f-a04a5e948964', N'HIGH', N'Visok', 3, CAST(N'2021-08-22T11:38:18.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[TaskPriority] ([Id], [Code], [Name], [PriorityLevel], [Created], [CreatedBy], [Modified], [ModifiedBy], [Active]) VALUES (N'86eedf7a-3d03-ec11-981f-a04a5e948964', N'HIGHEST', N'Najviši', 4, CAST(N'2021-08-22T11:38:34.0000000+00:00' AS DateTimeOffset), NULL, NULL, NULL, 1)
GO
INSERT [dbo].[TaskStatus] ([Id], [Code], [Name], [IsDefault], [Active], [Color]) VALUES (N'2b7294df-3d03-ec11-981f-a04a5e948964', N'TODO', N'TO - DO', 1, 1, N'#77FE00')
GO
INSERT [dbo].[TaskStatus] ([Id], [Code], [Name], [IsDefault], [Active], [Color]) VALUES (N'4799feeb-3d03-ec11-981f-a04a5e948964', N'IN_PROGRESS', N'U izradi', 0, 1, N'#0068FE')
GO
INSERT [dbo].[TaskStatus] ([Id], [Code], [Name], [IsDefault], [Active], [Color]) VALUES (N'25c2bff4-3d03-ec11-981f-a04a5e948964', N'DONE', N'Završeno', 0, 1, N'#FE00F2')
GO
INSERT [dbo].[EventType] ([Id], [Code], [Name], [IsDefault], [Active]) VALUES (N'349a9e9a-3e03-ec11-981f-a04a5e948964', N'CHECK_IN', N'Prijava', 1, 1)
GO
INSERT [dbo].[EventType] ([Id], [Code], [Name], [IsDefault], [Active]) VALUES (N'31d86fa1-3e03-ec11-981f-a04a5e948964', N'CHECK_OUT', N'Odjava', 0, 1)
GO
INSERT [dbo].[EventTypeTransition] ([Id], [EventTypeId], [NextEventTypeId], [Active], [Name], [Description]) VALUES (N'f6d46c15-3f03-ec11-981f-a04a5e948964', NULL, N'349a9e9a-3e03-ec11-981f-a04a5e948964', 1, N'NULL -> Prijava', N'NULL -> Prijava')
GO
INSERT [dbo].[EventTypeTransition] ([Id], [EventTypeId], [NextEventTypeId], [Active], [Name], [Description]) VALUES (N'96083927-3f03-ec11-981f-a04a5e948964', N'349a9e9a-3e03-ec11-981f-a04a5e948964', N'31d86fa1-3e03-ec11-981f-a04a5e948964', 1, N'Prijava -> Odjava', N'Prijava -> Odjava')
GO
INSERT [dbo].[EventTypeTransition] ([Id], [EventTypeId], [NextEventTypeId], [Active], [Name], [Description]) VALUES (N'581adf38-3f03-ec11-981f-a04a5e948964', N'31d86fa1-3e03-ec11-981f-a04a5e948964', N'349a9e9a-3e03-ec11-981f-a04a5e948964', 1, N'Odjava -> Prijava', N'Odjava -> Prijava')
GO
